@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/quality_s_and_con_ads') }}">Quality s and con ad</a> :
@endsection
@section("contentheader_description", $quality_s_and_con_ad->$view_col)
@section("section", "Quality s and con ads")
@section("section_url", url(config('laraadmin.adminRoute') . '/quality_s_and_con_ads'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Quality s and con ads Edit : ".$quality_s_and_con_ad->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($quality_s_and_con_ad, ['route' => [config('laraadmin.adminRoute') . '.quality_s_and_con_ads.update', $quality_s_and_con_ad->id ], 'method'=>'PUT', 'id' => 'quality_s_and_con_ad-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/quality_s_and_con_ads') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#quality_s_and_con_ad-edit-form").validate({
		
	});
});
</script>
@endpush
