@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests') }}">Prop sp and cost est</a> :
@endsection
@section("contentheader_description", $prop_sp_and_cost_est->$view_col)
@section("section", "Prop sp and cost ests")
@section("section_url", url(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Prop sp and cost ests Edit : ".$prop_sp_and_cost_est->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($prop_sp_and_cost_est, ['route' => [config('laraadmin.adminRoute') . '.prop_sp_and_cost_ests.update', $prop_sp_and_cost_est->id ], 'method'=>'PUT', 'id' => 'prop_sp_and_cost_est-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					@la_input($module, 'slogan_az')
					@la_input($module, 'slogan_en')
					@la_input($module, 'slogan_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#prop_sp_and_cost_est-edit-form").validate({
		
	});
});
</script>
@endpush
