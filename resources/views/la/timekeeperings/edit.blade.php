@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/timekeeperings') }}">Timekeepering</a> :
@endsection
@section("contentheader_description", $timekeepering->$view_col)
@section("section", "Timekeeperings")
@section("section_url", url(config('laraadmin.adminRoute') . '/timekeeperings'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Timekeeperings Edit : ".$timekeepering->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($timekeepering, ['route' => [config('laraadmin.adminRoute') . '.timekeeperings.update', $timekeepering->id ], 'method'=>'PUT', 'id' => 'timekeepering-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/timekeeperings') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#timekeepering-edit-form").validate({
		
	});
});
</script>
@endpush
