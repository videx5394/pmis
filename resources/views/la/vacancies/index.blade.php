@extends("la.layouts.app")

@section("contentheader_title", "Vacancies")
@section("contentheader_description", "Vacancies listing")
@section("section", "Vacancies")
@section("sub_section", "Listing")
@section("htmlheader_title", "Vacancies Listing")

@section("headerElems")
@la_access("Vacancies", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Vacancy</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Vacancies", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Vacancy</h4>
			</div>
			{!! Form::open(['action' => 'LA\VacanciesController@store', 'id' => 'vacancy-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'title_az')
					@la_input($module, 'title_en')
					@la_input($module, 'title_ru')
					@la_input($module, 'location_az')
					@la_input($module, 'location_en')
					@la_input($module, 'location_ru')
					@la_input($module, 'position_az')
					@la_input($module, 'position_en')
					@la_input($module, 'position_ru')
					@la_input($module, 'age')
					@la_input($module, 'education_az')
					@la_input($module, 'education_en')
					@la_input($module, 'education_ru')
					@la_input($module, 'experience_az')
					@la_input($module, 'experience_en')
					@la_input($module, 'experience_ru')
					@la_input($module, 'salary')
					@la_input($module, 'schedule_az')
					@la_input($module, 'schedule_en')
					@la_input($module, 'schedule_ru')
					@la_input($module, 'published_on')
					@la_input($module, 'expired_on')
					@la_input($module, 'meal')
					@la_input($module, 'transport')
					@la_input($module, 'description_az')
					@la_input($module, 'description_en')
					@la_input($module, 'description_ru')
					@la_input($module, 'requirements_az')
					@la_input($module, 'requirements_en')
					@la_input($module, 'requirements_ru')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/vacancy_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#vacancy-add-form").validate({
		
	});
});
</script>
@endpush
