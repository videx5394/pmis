@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/planning_and_scheds') }}">Planning and sched</a> :
@endsection
@section("contentheader_description", $planning_and_sched->$view_col)
@section("section", "Planning and scheds")
@section("section_url", url(config('laraadmin.adminRoute') . '/planning_and_scheds'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Planning and scheds Edit : ".$planning_and_sched->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($planning_and_sched, ['route' => [config('laraadmin.adminRoute') . '.planning_and_scheds.update', $planning_and_sched->id ], 'method'=>'PUT', 'id' => 'planning_and_sched-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					@la_input($module, 'slogan_az')
					@la_input($module, 'slogan_en')
					@la_input($module, 'slogan_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/planning_and_scheds') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#planning_and_sched-edit-form").validate({
		
	});
});
</script>
@endpush
