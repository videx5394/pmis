@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/team_members') }}">Team member</a> :
@endsection
@section("contentheader_description", $team_member->$view_col)
@section("section", "Team members")
@section("section_url", url(config('laraadmin.adminRoute') . '/team_members'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Team members Edit : ".$team_member->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($team_member, ['route' => [config('laraadmin.adminRoute') . '.team_members.update', $team_member->id ], 'method'=>'PUT', 'id' => 'team_member-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name_az')
					@la_input($module, 'name_en')
					@la_input($module, 'name_ru')
					@la_input($module, 'profile_image')
					@la_input($module, 'mail')
					@la_input($module, 'linkedIn')
					@la_input($module, 'position_az')
					@la_input($module, 'position_en')
					@la_input($module, 'position_ru')
					@la_input($module, 'member_type')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/team_members') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#team_member-edit-form").validate({
		
	});
});
</script>
@endpush
