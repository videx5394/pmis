@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/cost_contr_and_subs') }}">Cost contr and sub</a> :
@endsection
@section("contentheader_description", $cost_contr_and_sub->$view_col)
@section("section", "Cost contr and subs")
@section("section_url", url(config('laraadmin.adminRoute') . '/cost_contr_and_subs'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Cost contr and subs Edit : ".$cost_contr_and_sub->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($cost_contr_and_sub, ['route' => [config('laraadmin.adminRoute') . '.cost_contr_and_subs.update', $cost_contr_and_sub->id ], 'method'=>'PUT', 'id' => 'cost_contr_and_sub-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					@la_input($module, 'slogan_az')
					@la_input($module, 'slogan_en')
					@la_input($module, 'slogan_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/cost_contr_and_subs') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#cost_contr_and_sub-edit-form").validate({
		
	});
});
</script>
@endpush
