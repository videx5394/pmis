@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/about_us_shorts') }}">About us short</a> :
@endsection
@section("contentheader_description", $about_us_short->$view_col)
@section("section", "About us shorts")
@section("section_url", url(config('laraadmin.adminRoute') . '/about_us_shorts'))
@section("sub_section", "Edit")

@section("htmlheader_title", "About us shorts Edit : ".$about_us_short->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($about_us_short, ['route' => [config('laraadmin.adminRoute') . '.about_us_shorts.update', $about_us_short->id ], 'method'=>'PUT', 'id' => 'about_us_short-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/about_us_shorts') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#about_us_short-edit-form").validate({
		
	});
});
</script>
@endpush
