<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
	@include('la.layouts.partials.htmlheader')
@show
<body class="{{ LAConfigs::getByKey('skin') }} {{ LAConfigs::getByKey('layout') }} @if(LAConfigs::getByKey('layout') == 'sidebar-mini') sidebar-collapse @endif" bsurl="{{ url('') }}" adminRoute="{{ config('laraadmin.adminRoute') }}">
<div class="wrapper">

	@include('la.layouts.partials.mainheader')

	@if(LAConfigs::getByKey('layout') != 'layout-top-nav')
		@include('la.layouts.partials.sidebar')
	@endif

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		@if(LAConfigs::getByKey('layout') == 'layout-top-nav') <div class="container"> @endif
		@if(!isset($no_header))
			@include('la.layouts.partials.contentheader')
		@endif
		
		<!-- Main content -->
		<section class="content {{ $no_padding or '' }}">
			<!-- Your Page Content Here -->
			@yield('main-content')
		</section><!-- /.content -->

		@if(LAConfigs::getByKey('layout') == 'layout-top-nav') </div> @endif
	</div><!-- /.content-wrapper -->

	@include('la.layouts.partials.controlsidebar')

	@include('la.layouts.partials.footer')

</div><!-- ./wrapper -->

@include('la.layouts.partials.file_manager')

@section('scripts')
	@include('la.layouts.partials.scripts')
@show

<script>
	$(document).ready(function () {
		$('[name="text_az"]').wysiwyg({"font-styles": false});
		$('[name="text_ru"]').wysiwyg({"font-styles": false});
		$('[name="text_en"]').wysiwyg({"font-styles": false});
		if($('[name="position_az"]').length>0)
		{
			$('[name="position_az"]').wysiwyg({"font-styles": false});
			$('[name="position_ru"]').wysiwyg({"font-styles": false});
			$('[name="position_en"]').wysiwyg({"font-styles": false});
		}
	});

	function removeMultiplicity(name) {
		if(name.endsWith('ses'))
		{
			name = name.substr(0,(name.length - 2));
		}
		else if(name.endsWith('s'))
			name = name.substr(0,(name.length - 1));
		return name;
	}

	$('#contentheader_title').text(removeMultiplicity($('#contentheader_title').text().trim()));

</script>

</body>
</html>
