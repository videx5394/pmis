@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/contact_infos') }}">Contact info</a> :
@endsection
@section("contentheader_description", $contact_info->$view_col)
@section("section", "Contact infos")
@section("section_url", url(config('laraadmin.adminRoute') . '/contact_infos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Contact infos Edit : ".$contact_info->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($contact_info, ['route' => [config('laraadmin.adminRoute') . '.contact_infos.update', $contact_info->id ], 'method'=>'PUT', 'id' => 'contact_info-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'phone')
					@la_input($module, 'email')
					@la_input($module, 'linkedin')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/contact_infos') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#contact_info-edit-form").validate({
		
	});
});
</script>
@endpush
