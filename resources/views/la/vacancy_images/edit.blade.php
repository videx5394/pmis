@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/vacancy_images') }}">Vacancy image</a> :
@endsection
@section("contentheader_description", $vacancy_image->$view_col)
@section("section", "Vacancy images")
@section("section_url", url(config('laraadmin.adminRoute') . '/vacancy_images'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Vacancy images Edit : ".$vacancy_image->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($vacancy_image, ['route' => [config('laraadmin.adminRoute') . '.vacancy_images.update', $vacancy_image->id ], 'method'=>'PUT', 'id' => 'vacancy_image-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/vacancy_images') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#vacancy_image-edit-form").validate({
		
	});
});
</script>
@endpush
