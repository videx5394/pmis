@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/reporting_and_autos') }}">Reporting and auto</a> :
@endsection
@section("contentheader_description", $reporting_and_auto->$view_col)
@section("section", "Reporting and autos")
@section("section_url", url(config('laraadmin.adminRoute') . '/reporting_and_autos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Reporting and autos Edit : ".$reporting_and_auto->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($reporting_and_auto, ['route' => [config('laraadmin.adminRoute') . '.reporting_and_autos.update', $reporting_and_auto->id ], 'method'=>'PUT', 'id' => 'reporting_and_auto-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'text_az')
					@la_input($module, 'text_en')
					@la_input($module, 'text_ru')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/reporting_and_autos') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#reporting_and_auto-edit-form").validate({
		
	});
});
</script>
@endpush
