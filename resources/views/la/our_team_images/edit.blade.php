@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/our_team_images') }}">Our team image</a> :
@endsection
@section("contentheader_description", $our_team_image->$view_col)
@section("section", "Our team images")
@section("section_url", url(config('laraadmin.adminRoute') . '/our_team_images'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Our team images Edit : ".$our_team_image->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($our_team_image, ['route' => [config('laraadmin.adminRoute') . '.our_team_images.update', $our_team_image->id ], 'method'=>'PUT', 'id' => 'our_team_image-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'image')
					@la_input($module, 'leadership_image')
					@la_input($module, 'people_image')
					@la_input($module, 'expert_image')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/our_team_images') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#our_team_image-edit-form").validate({
		
	});
});
</script>
@endpush
