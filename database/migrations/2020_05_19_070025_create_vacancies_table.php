<?php
/**
 * Migration genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Dwij\Laraadmin\Models\Module;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Module::generate("Vacancies", 'vacancies', 'title', 'fa-cube', [
            ["title_az", "title az", "TextField", false, "", 0, 256, false],
            ["title_en", "title en", "TextField", false, "", 0, 256, false],
            ["title_ru", "title ru", "TextField", false, "", 0, 256, false],
            ["location_az", "location az", "TextField", false, "", 0, 256, false],
            ["location_en", "location en ", "TextField", false, "", 0, 256, false],
            ["location_ru", "location ru", "TextField", false, "", 0, 256, false],
            ["position_az", "position az", "TextField", false, "", 0, 256, false],
            ["position_en", "position en", "TextField", false, "", 0, 256, false],
            ["position_ru", "position ru", "TextField", false, "", 0, 256, false],
            ["age", "age", "TextField", false, "", 0, 256, false],
            ["education_az", "education az", "TextField", false, "", 0, 256, false],
            ["education_en", "education en", "TextField", false, "", 0, 256, false],
            ["education_ru", "education ru", "TextField", false, "", 0, 256, false],
            ["experience_az", "experience az", "TextField", false, "", 0, 256, false],
            ["experience_en", "experience en", "TextField", false, "", 0, 256, false],
            ["experience_ru", "experience ru", "TextField", false, "", 0, 256, false],
            ["salary", "salary", "TextField", false, "", 0, 256, false],
            ["schedule_az", "schedule az", "TextField", false, "", 0, 256, false],
            ["schedule_en", "schedule en", "TextField", false, "", 0, 256, false],
            ["schedule_ru", "schedule ru", "TextField", false, "", 0, 256, false],
            ["published_on", "published on", "TextField", false, "", 0, 256, false],
            ["expired_on", "expired_on", "TextField", false, "", 0, 256, false],
            ["meal", "meal", "Dropdown", false, "", 0, 256, false, ["yes","no"]],
            ["transport", "transport", "Dropdown", false, "", 0, 0, false, ["yes","no"]],
            ["description_az", "description az", "Textarea", false, "", 0, 0, false],
            ["description_en", "description en", "Textarea", false, "", 0, 0, false],
            ["description_ru", "description ru", "Textarea", false, "", 0, 0, false],
            ["requirements_az", "requirements az", "Textarea", false, "", 0, 0, false],
            ["requirements_en", "requirements en", "Textarea", false, "", 0, 0, false],
            ["requirements_ru", "requirements ru", "Textarea", false, "", 0, 0, false],
        ]);
		
		/*
		Row Format:
		["field_name_db", "Label", "UI Type", "Unique", "Default_Value", "min_length", "max_length", "Required", "Pop_values"]
        Module::generate("Module_Name", "Table_Name", "view_column_name" "Fields_Array");
        
		Module::generate("Books", 'books', 'name', [
            ["address",     "Address",      "Address",  false, "",          0,  1000,   true],
            ["restricted",  "Restricted",   "Checkbox", false, false,       0,  0,      false],
            ["price",       "Price",        "Currency", false, 0.0,         0,  0,      true],
            ["date_release", "Date of Release", "Date", false, "date('Y-m-d')", 0, 0,   false],
            ["time_started", "Start Time",  "Datetime", false, "date('Y-m-d H:i:s')", 0, 0, false],
            ["weight",      "Weight",       "Decimal",  false, 0.0,         0,  20,     true],
            ["publisher",   "Publisher",    "Dropdown", false, "Marvel",    0,  0,      false, ["Bloomsbury","Marvel","Universal"]],
            ["publisher",   "Publisher",    "Dropdown", false, 3,           0,  0,      false, "@publishers"],
            ["email",       "Email",        "Email",    false, "",          0,  0,      false],
            ["file",        "File",         "File",     false, "",          0,  1,      false],
            ["files",       "Files",        "Files",    false, "",          0,  10,     false],
            ["weight",      "Weight",       "Float",    false, 0.0,         0,  20.00,  true],
            ["biography",   "Biography",    "HTML",     false, "<p>This is description</p>", 0, 0, true],
            ["profile_image", "Profile Image", "Image", false, "img_path.jpg", 0, 250,  false],
            ["pages",       "Pages",        "Integer",  false, 0,           0,  5000,   false],
            ["mobile",      "Mobile",       "Mobile",   false, "+91  8888888888", 0, 20,false],
            ["media_type",  "Media Type",   "Multiselect", false, ["Audiobook"], 0, 0,  false, ["Print","Audiobook","E-book"]],
            ["media_type",  "Media Type",   "Multiselect", false, [2,3],    0,  0,      false, "@media_types"],
            ["name",        "Name",         "Name",     false, "John Doe",  5,  250,    true],
            ["password",    "Password",     "Password", false, "",          6,  250,    true],
            ["status",      "Status",       "Radio",    false, "Published", 0,  0,      false, ["Draft","Published","Unpublished"]],
            ["author",      "Author",       "String",   false, "JRR Tolkien", 0, 250,   true],
            ["genre",       "Genre",        "Taginput", false, ["Fantacy","Adventure"], 0, 0, false],
            ["description", "Description",  "Textarea", false, "",          0,  1000,   false],
            ["short_intro", "Introduction", "TextField",false, "",          5,  250,    true],
            ["website",     "Website",      "URL",      false, "http://dwij.in", 0, 0,  false],
        ]);
		*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('vacancies')) {
            Schema::drop('vacancies');
        }
    }
}
