<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Prop_sp_and_cost_est;

class Prop_sp_and_cost_estsController extends Controller
{
	public $show_action = true;
	public $view_col = 'text_az';
	public $listing_cols = ['id', 'image', 'text_az', 'text_en', 'text_ru', 'slogan_az', 'slogan_en', 'slogan_ru'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Prop_sp_and_cost_ests', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Prop_sp_and_cost_ests', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Prop_sp_and_cost_ests.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Prop_sp_and_cost_ests');
		
		if(Module::hasAccess($module->id)) {
			return View('la.prop_sp_and_cost_ests.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new prop_sp_and_cost_est.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created prop_sp_and_cost_est in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Prop_sp_and_cost_ests", "create")) {
		
			$rules = Module::validateRules("Prop_sp_and_cost_ests", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Prop_sp_and_cost_ests", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.prop_sp_and_cost_ests.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified prop_sp_and_cost_est.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Prop_sp_and_cost_ests", "view")) {
			
			$prop_sp_and_cost_est = Prop_sp_and_cost_est::find($id);
			if(isset($prop_sp_and_cost_est->id)) {
				$module = Module::get('Prop_sp_and_cost_ests');
				$module->row = $prop_sp_and_cost_est;
				
				return view('la.prop_sp_and_cost_ests.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('prop_sp_and_cost_est', $prop_sp_and_cost_est);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("prop_sp_and_cost_est"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified prop_sp_and_cost_est.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Prop_sp_and_cost_ests", "edit")) {			
			$prop_sp_and_cost_est = Prop_sp_and_cost_est::find($id);
			if(isset($prop_sp_and_cost_est->id)) {	
				$module = Module::get('Prop_sp_and_cost_ests');
				
				$module->row = $prop_sp_and_cost_est;
				
				return view('la.prop_sp_and_cost_ests.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('prop_sp_and_cost_est', $prop_sp_and_cost_est);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("prop_sp_and_cost_est"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified prop_sp_and_cost_est in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Prop_sp_and_cost_ests", "edit")) {
			
			$rules = Module::validateRules("Prop_sp_and_cost_ests", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Prop_sp_and_cost_ests", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.prop_sp_and_cost_ests.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified prop_sp_and_cost_est from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Prop_sp_and_cost_ests", "delete")) {
			Prop_sp_and_cost_est::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.prop_sp_and_cost_ests.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('prop_sp_and_cost_ests')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Prop_sp_and_cost_ests');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Prop_sp_and_cost_ests", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Prop_sp_and_cost_ests", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.prop_sp_and_cost_ests.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
