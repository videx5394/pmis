<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Cost_contr_and_sub;

class Cost_contr_and_subsController extends Controller
{
	public $show_action = true;
	public $view_col = 'image';
	public $listing_cols = ['id', 'image', 'text_az', 'text_en', 'text_ru', 'slogan_az', 'slogan_en', 'slogan_ru'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Cost_contr_and_subs', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Cost_contr_and_subs', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Cost_contr_and_subs.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Cost_contr_and_subs');
		
		if(Module::hasAccess($module->id)) {
			return View('la.cost_contr_and_subs.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new cost_contr_and_sub.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created cost_contr_and_sub in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Cost_contr_and_subs", "create")) {
		
			$rules = Module::validateRules("Cost_contr_and_subs", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Cost_contr_and_subs", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.cost_contr_and_subs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified cost_contr_and_sub.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Cost_contr_and_subs", "view")) {
			
			$cost_contr_and_sub = Cost_contr_and_sub::find($id);
			if(isset($cost_contr_and_sub->id)) {
				$module = Module::get('Cost_contr_and_subs');
				$module->row = $cost_contr_and_sub;
				
				return view('la.cost_contr_and_subs.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('cost_contr_and_sub', $cost_contr_and_sub);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("cost_contr_and_sub"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified cost_contr_and_sub.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Cost_contr_and_subs", "edit")) {			
			$cost_contr_and_sub = Cost_contr_and_sub::find($id);
			if(isset($cost_contr_and_sub->id)) {	
				$module = Module::get('Cost_contr_and_subs');
				
				$module->row = $cost_contr_and_sub;
				
				return view('la.cost_contr_and_subs.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('cost_contr_and_sub', $cost_contr_and_sub);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("cost_contr_and_sub"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified cost_contr_and_sub in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Cost_contr_and_subs", "edit")) {
			
			$rules = Module::validateRules("Cost_contr_and_subs", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Cost_contr_and_subs", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.cost_contr_and_subs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified cost_contr_and_sub from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Cost_contr_and_subs", "delete")) {
			Cost_contr_and_sub::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.cost_contr_and_subs.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('cost_contr_and_subs')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Cost_contr_and_subs');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/cost_contr_and_subs/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Cost_contr_and_subs", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/cost_contr_and_subs/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Cost_contr_and_subs", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.cost_contr_and_subs.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
