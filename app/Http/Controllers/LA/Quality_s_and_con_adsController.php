<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Quality_s_and_con_ad;

class Quality_s_and_con_adsController extends Controller
{
	public $show_action = true;
	public $view_col = 'image';
	public $listing_cols = ['id', 'image', 'text_az', 'text_en', 'text_ru'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Quality_s_and_con_ads', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Quality_s_and_con_ads', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Quality_s_and_con_ads.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Quality_s_and_con_ads');
		
		if(Module::hasAccess($module->id)) {
			return View('la.quality_s_and_con_ads.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new quality_s_and_con_ad.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created quality_s_and_con_ad in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Quality_s_and_con_ads", "create")) {
		
			$rules = Module::validateRules("Quality_s_and_con_ads", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Quality_s_and_con_ads", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.quality_s_and_con_ads.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified quality_s_and_con_ad.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Quality_s_and_con_ads", "view")) {
			
			$quality_s_and_con_ad = Quality_s_and_con_ad::find($id);
			if(isset($quality_s_and_con_ad->id)) {
				$module = Module::get('Quality_s_and_con_ads');
				$module->row = $quality_s_and_con_ad;
				
				return view('la.quality_s_and_con_ads.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('quality_s_and_con_ad', $quality_s_and_con_ad);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("quality_s_and_con_ad"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified quality_s_and_con_ad.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Quality_s_and_con_ads", "edit")) {			
			$quality_s_and_con_ad = Quality_s_and_con_ad::find($id);
			if(isset($quality_s_and_con_ad->id)) {	
				$module = Module::get('Quality_s_and_con_ads');
				
				$module->row = $quality_s_and_con_ad;
				
				return view('la.quality_s_and_con_ads.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('quality_s_and_con_ad', $quality_s_and_con_ad);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("quality_s_and_con_ad"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified quality_s_and_con_ad in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Quality_s_and_con_ads", "edit")) {
			
			$rules = Module::validateRules("Quality_s_and_con_ads", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Quality_s_and_con_ads", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.quality_s_and_con_ads.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified quality_s_and_con_ad from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Quality_s_and_con_ads", "delete")) {
			Quality_s_and_con_ad::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.quality_s_and_con_ads.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('quality_s_and_con_ads')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Quality_s_and_con_ads');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/quality_s_and_con_ads/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Quality_s_and_con_ads", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/quality_s_and_con_ads/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Quality_s_and_con_ads", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.quality_s_and_con_ads.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
