<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\Contact_infosController@index')->name('first_page');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\Contact_infosController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');



	/* ================== Prop_sp_and_cost_ests ================== */
	Route::resource(config('laraadmin.adminRoute') . '/prop_sp_and_cost_ests', 'LA\Prop_sp_and_cost_estsController');
	Route::get(config('laraadmin.adminRoute') . '/prop_sp_and_cost_est_dt_ajax', 'LA\Prop_sp_and_cost_estsController@dtajax');

	/* ================== Planning_and_scheds ================== */
	Route::resource(config('laraadmin.adminRoute') . '/planning_and_scheds', 'LA\Planning_and_schedsController');
	Route::get(config('laraadmin.adminRoute') . '/planning_and_sched_dt_ajax', 'LA\Planning_and_schedsController@dtajax');

	/* ================== Quality_s_and_con_ads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/quality_s_and_con_ads', 'LA\Quality_s_and_con_adsController');
	Route::get(config('laraadmin.adminRoute') . '/quality_s_and_con_ad_dt_ajax', 'LA\Quality_s_and_con_adsController@dtajax');

	/* ================== Cost_contr_and_subs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/cost_contr_and_subs', 'LA\Cost_contr_and_subsController');
	Route::get(config('laraadmin.adminRoute') . '/cost_contr_and_sub_dt_ajax', 'LA\Cost_contr_and_subsController@dtajax');

	/* ================== Timekeeperings ================== */
	Route::resource(config('laraadmin.adminRoute') . '/timekeeperings', 'LA\TimekeeperingsController');
	Route::get(config('laraadmin.adminRoute') . '/timekeepering_dt_ajax', 'LA\TimekeeperingsController@dtajax');


	/* ================== Reporting_and_autos ================== */
	Route::resource(config('laraadmin.adminRoute') . '/reporting_and_autos', 'LA\Reporting_and_autosController');
	Route::get(config('laraadmin.adminRoute') . '/reporting_and_auto_dt_ajax', 'LA\Reporting_and_autosController@dtajax');

	/* ================== Abouts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/abouts', 'LA\AboutsController');
	Route::get(config('laraadmin.adminRoute') . '/about_dt_ajax', 'LA\AboutsController@dtajax');

	/* ================== Team_members ================== */
	Route::resource(config('laraadmin.adminRoute') . '/team_members', 'LA\Team_membersController');
	Route::get(config('laraadmin.adminRoute') . '/team_member_dt_ajax', 'LA\Team_membersController@dtajax');

	/* ================== Clients ================== */
	Route::resource(config('laraadmin.adminRoute') . '/clients', 'LA\ClientsController');
	Route::get(config('laraadmin.adminRoute') . '/client_dt_ajax', 'LA\ClientsController@dtajax');

	/* ================== Contact_infos ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contact_infos', 'LA\Contact_infosController');
	Route::get(config('laraadmin.adminRoute') . '/contact_info_dt_ajax', 'LA\Contact_infosController@dtajax');


	/* ================== Our_team_images ================== */
	Route::resource(config('laraadmin.adminRoute') . '/our_team_images', 'LA\Our_team_imagesController');
	Route::get(config('laraadmin.adminRoute') . '/our_team_image_dt_ajax', 'LA\Our_team_imagesController@dtajax');


	/* ================== Contact_uses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contact_uses', 'LA\Contact_usesController');
	Route::get(config('laraadmin.adminRoute') . '/contact_us_dt_ajax', 'LA\Contact_usesController@dtajax');

	/* ================== Homes ================== */
	Route::resource(config('laraadmin.adminRoute') . '/homes', 'LA\HomesController');
	Route::get(config('laraadmin.adminRoute') . '/home_dt_ajax', 'LA\HomesController@dtajax');

	/* ================== About_us_shorts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/about_us_shorts', 'LA\About_us_shortsController');
	Route::get(config('laraadmin.adminRoute') . '/about_us_short_dt_ajax', 'LA\About_us_shortsController@dtajax');



	/* ================== Vacancies ================== */
	Route::resource(config('laraadmin.adminRoute') . '/vacancies', 'LA\VacanciesController');
	Route::get(config('laraadmin.adminRoute') . '/vacancy_dt_ajax', 'LA\VacanciesController@dtajax');

	/* ================== Vacancy_images ================== */
	Route::resource(config('laraadmin.adminRoute') . '/vacancy_images', 'LA\Vacancy_imagesController');
	Route::get(config('laraadmin.adminRoute') . '/vacancy_image_dt_ajax', 'LA\Vacancy_imagesController@dtajax');

	/* ================== Projects ================== */
	Route::resource(config('laraadmin.adminRoute') . '/projects', 'LA\ProjectsController');
	Route::get(config('laraadmin.adminRoute') . '/project_dt_ajax', 'LA\ProjectsController@dtajax');
});
